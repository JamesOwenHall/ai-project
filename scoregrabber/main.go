package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	for i := 0; i < 82; i++ {
		url := fmt.Sprintf("http://www.nhl.com/ice/gamestats.htm?fetchKey=20142ALLSATALL&viewName=gameSummary&sort=game.gameDate&pg=%d", i+1)

		resp, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		filename := fmt.Sprintf("pages/page%d.txt", i+1)
		err = ioutil.WriteFile(filename, body, 0755)
	}
}

package hockeyprediction;

import org.neuroph.core.data.DataSet;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;

public class Trainer {
	public static MultiLayerPerceptron getNetwork(DataSet trainingData) {
		MultiLayerPerceptron result = new MultiLayerPerceptron(trainingData.getInputSize(), trainingData.getInputSize(), trainingData.getOutputSize());
		result.setLearningRule(new MomentumBackpropagation());
		((MomentumBackpropagation)result.getLearningRule()).setMomentum(0.8);
		result.getLearningRule().setMaxError(0.05);
		result.getLearningRule().setLearningRate(0.5);
		
		result.learn(trainingData);
		
		return result;
	}
}

package hockeyprediction;

import java.util.Iterator;
import java.util.LinkedList;

public class Utils {
	/**
	 * Converts a linked list of Double objects to an array of doubles
	 * @param list
	 * @return
	 */
	public static double[] doubleListToArray(LinkedList<Double> list) {
		double[] result = new double[list.size()];
		
		Iterator<Double> iterator = list.iterator();
		for (int i = 0; i < result.length; i++) {
			result[i] = (double)iterator.next();
		}
		
		return result;
	}
	
	/**
	 * Converts a linked list of Integer objects to an array of ints
	 * @param list
	 * @return
	 */
	public static int[] integerListToArray(LinkedList<Integer> list) {
		int[] result = new int[list.size()];
		
		Iterator<Integer> iterator = list.iterator();
		for (int i = 0; i < result.length; i++) {
			result[i] = (int)iterator.next();
		}
		
		return result;
	}
	
	/**
	 * Generates the power set of integers from zero to the specified size.
	 * E.G. powerSet(1) = [[], [0], [1], [0, 1]]
	 * @param size
	 * @return
	 */
	public static int[][] powerSet(int size) {
		int[][] result = new int[(int)Math.pow(2, size)][];
		
		for (int i = 0; i < result.length; i++) {
			LinkedList<Integer> list = new LinkedList<Integer>();
			
			int mask = 1;
			for (int j = 0; j < size; j++) {
				if ((i & mask) != 0) {
					list.add(j);
				}
				
				mask = mask << 1;
			}
			
			result[i] = integerListToArray(list);
		}
		
		return result;
	}
}

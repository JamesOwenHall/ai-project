package hockeyprediction.input;

import hockeyprediction.DataModel;
import hockeyprediction.GameResult;

import java.sql.SQLException;

public interface PredictionInput {
	public double generateValue(String team, GameResult[] results, int index) throws SQLException;
	
	public static PredictionInput[] allInputs(DataModel model) {
		return new PredictionInput[]{
			new LastGameDifferenceInput(model),
			new LastGameGoalInput(model),
			new LastNGamesGoalInput(model),
			new OpponentInput(model)
		};
	}
}

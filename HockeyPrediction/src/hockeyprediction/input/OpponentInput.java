package hockeyprediction.input;

import hockeyprediction.DataModel;
import hockeyprediction.GameResult;

import java.sql.SQLException;

public class OpponentInput implements PredictionInput {
	DataModel model;
	
	public OpponentInput(DataModel model) {
		this.model = model;
	}
	
	@Override
	public double generateValue(String team, GameResult[] games, int index) throws SQLException {
		float returnData = 0;

		if (games[index].homeTeam.equals(team)) {
			returnData = model.getTeamID(games[index].visitingTeam);
		} else {
			returnData = model.getTeamID(games[index].homeTeam);
		}

		return returnData;
	}
	
	@Override
	public String toString() {
		return "Opponent";
	}
}

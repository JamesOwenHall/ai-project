package hockeyprediction.input;

import hockeyprediction.DataModel;
import hockeyprediction.GameResult;

import java.sql.SQLException;

public class LastGameDifferenceInput implements PredictionInput {
	DataModel model;
	
	public LastGameDifferenceInput(DataModel model) {
		this.model = model;
	}

	@Override
	public double generateValue(String team, GameResult[] results, int index) throws SQLException {
		if (index == 0) {
			return 0;
		}
		
		GameResult lastGame = results[index - 1];
		
		if (lastGame.homeTeam.equals(team)) {
			return (double)(lastGame.homeGoals - lastGame.visitingGoals) / 10.0;
		} else {
			return (double)(lastGame.visitingGoals - lastGame.homeGoals) / 10.0;
		}
	}
	
	@Override
	public String toString() {
		return "Last game difference";
	}
}

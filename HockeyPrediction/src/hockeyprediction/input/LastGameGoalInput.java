package hockeyprediction.input;

import hockeyprediction.DataModel;
import hockeyprediction.GameResult;

import java.sql.SQLException;

public class LastGameGoalInput implements PredictionInput {
	DataModel model;
	
	public LastGameGoalInput(DataModel model) {
		this.model = model;
	}

	@Override
	public double generateValue(String team, GameResult[] results, int index) throws SQLException {
		if (index == 0) {
			return 0;
		}
		
		GameResult lastGame = results[index - 1];
		
		if (lastGame.homeTeam.equals(team)) {
			return (double)lastGame.homeGoals / 10.0;
		} else {
			return (double)lastGame.visitingGoals / 10.0;
		}
	}
	
	@Override
	public String toString() {
		return "Last game goals";
	}
}

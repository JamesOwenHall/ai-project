package hockeyprediction;

import hockeyprediction.input.PredictionInput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

public class HockeyPrediction {
	final static LocalDate MIN_DATE = LocalDate.of(2013, 10, 10);
	public static LocalDate TrainingDate = LocalDate.of(2014, 1, 1);

	DataModel model;
	LocalDate simDate;
	Predictor predictor;

	public HockeyPrediction() throws SQLException {
		model = new DataModel();
		simDate = LocalDate.now();
		predictor = new MultiPredictor(model, PredictionInput.allInputs(model), simDate);
	}

	/**
	 * Executes the given command if it exists.
	 * 
	 * @param command
	 * @return True if command exists, false otherwise.
	 */
	public boolean executeCommand(String command) throws SQLException {
		String[] components = command.split("\\s+");

		switch (components[0].toLowerCase()) {
		case "":
			return true;

		case "date":
			setDate(components);
			return true;
			
		case "help":
			showHelp();
			return true;

		case "list":
			listTeamNames(components);
			return true;

		case "predict":
			predict(components);
			return true;

		default:
			return false;
		}
	}

	public void setDate(String[] components) throws SQLException {
		if (components.length != 4) {
			System.err.println("Usage: date <year> <month> <day>");
			return;
		}

		int year = 0;
		int month = 0;
		int day = 0;

		try {
			year = Integer.parseInt(components[1]);
			month = Integer.parseInt(components[2]);
			day = Integer.parseInt(components[3]);
		} catch (NumberFormatException e) {
			System.err.println("Dates must be represented as numbers");
			return;
		}

		LocalDate newDate;

		try {
			newDate = LocalDate.of(year, month, day);
		} catch (DateTimeException e) {
			System.err.println("Invalid date");
			return;
		}

		if (newDate.isBefore(MIN_DATE)) {
			System.err.println("Only dates after " + MIN_DATE + " are allowed");
			return;
		}

		simDate = newDate;

		System.out.println("Date set to " + simDate);
	}
	
	public void showHelp() {
		System.out.println("Available commands:" +
				"\n\tdate    - Sets the date of the prediction.  No games after this date will be used while training." + 
				"\n\tlist    - Shows the list of all team names." +
				"\n\tpredict - Makes a prediction for the goals scored by the given teams." +
				"\n\tquit    - Quits the application");
	}

	public void listTeamNames(String[] components) throws SQLException {
		if (components.length != 1) {
			System.err.println("Usage: list");
			return;
		}

		String[] teams = model.getTeamNames();

		for (String team : teams) {
			System.out.println(team);
		}
	}

	public void predict(String[] components) throws SQLException {
		if (components.length != 3) {
			System.err.println("Usage: predict <team1> <team2>");
			return;
		}

		String team1 = components[1].toUpperCase();
		String team2 = components[2].toUpperCase();

		if (!model.teamExists(team1)) {
			System.err.println("Team " + team1 + " does not exist.");
			return;
		}
		if (!model.teamExists(team2)) {
			System.err.println("Team " + team2 + " does not exist.");
			return;
		}

		double[] prediction = predictor.predict(team1, team2);
		System.out.printf("The predicted score is: %d - %d.\n", Math.round(prediction[0]), Math.round(prediction[1]));
	}

	public static void main(String[] args) throws IOException {
		HockeyPrediction hp = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));

		try {
			hp = new HockeyPrediction();
		} catch (SQLException e) {
			System.err.println("Unable to connect to the database");
			System.exit(1);
		}

		String command;

		while (true) {
			System.out.print("HockeyPrediction> ");
			command = reader.readLine();

			if (command.equalsIgnoreCase("quit")
					|| command.equalsIgnoreCase("q")) {
				return;
			}

			boolean commandExists = false;
			try {
				commandExists = hp.executeCommand(command);
			} catch (SQLException e) {
				System.err.println("Unable to connect to database");
				continue;
			}

			if (!commandExists) {
				System.err.println("Unrecognized command: " + command);
			}
		}
	}
}

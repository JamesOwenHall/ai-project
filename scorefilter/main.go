package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
)

func main() {
	for i := 1; i <= 82; i++ {
		filename := fmt.Sprintf("pages/page%d.txt", i)

		page, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}

		e1 := regexp.MustCompile("\n")
		nolines := e1.ReplaceAllString(string(page), " ")

		e2 := regexp.MustCompile("<table summary.*</table>")
		table := e2.FindString(nolines)

		e3 := regexp.MustCompile("<tbody>.*</tbody>")
		tbody := e3.FindString(table)

		e4 := regexp.MustCompile(`<(\w+)(\s+\w+="[a-zA-Z0-9:;/.\-\s()?=]+")*>`)
		stripped := e4.ReplaceAllString(tbody, "<${1}>")

		e5 := regexp.MustCompile("<a>|</a>")
		nolinks := e5.ReplaceAllString(stripped, "")

		e6 := regexp.MustCompile("<tr>.*?</tr>")
		e7 := regexp.MustCompile("<td>(.*?)</td>")

		rows := e6.FindAllString(nolinks, -1)
		for _, row := range rows {
			cells := e7.FindAllStringSubmatch(row, -1)

			for i, cell := range cells {
				fmt.Printf(`"%s"`, cell[1])

				if i+1 != len(cells) {
					fmt.Print(",")
				}
			}

			fmt.Println()
		}
	}
}

package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var nameExpansions = map[string]string{
	"ANA": "ANAHEIM",
	"BOS": "BOSTON",
	"BUF": "BUFFALO",
	"CAR": "CAROLINA",
	"CBJ": "COLUMBUS",
	"CGY": "CALGARY",
	"CHI": "CHICAGO",
	"COL": "COLORADO",
	"DAL": "DALLAS",
	"DET": "DETROIT",
	"EDM": "EDMONTON",
	"FLA": "FLORIDA",
	"LAK": "LOS_ANGELES",
	"MTL": "MONTREAL",
	"MIN": "MINNESOTA",
	"NJD": "NEW_JERSEY",
	"NSH": "NASHVILLE",
	"NYI": "NY_ISLANDERS",
	"NYR": "NY_RANGERS",
	"OTT": "OTTAWA",
	"PHI": "PHILADELPHIA",
	"PHX": "PHOENIX",
	"PIT": "PITTSBURGH",
	"SJS": "SAN_JOSE",
	"STL": "ST_LOUIS",
	"TBL": "TAMPA_BAY",
	"TOR": "TORONTO",
	"VAN": "VANCOUVER",
	"WPG": "WINNIPEG",
	"WSH": "WASHINGTON",
}

func main() {
	records := getRecords("../data.csv")

	db, err := sql.Open("mysql", "root@/ainn")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// iterate in reverse order
	for i := len(records) - 1; i >= 0; i-- {
		if records[i][4] == "H" {
			insertRecord(db, records[i])
		}
	}
}

func getRecords(filename string) [][]string {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	csvReader := csv.NewReader(file)

	records, err := csvReader.ReadAll()
	if err != nil {
		panic(err)
	}

	return records
}

func insertRecord(db *sql.DB, r []string) {
	const query = `INSERT INTO game_history VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

	fixOTScore(r)

	_, err := db.Exec(query, formatDate(r[0]), formatName(r[1]), expandName(r[5]), r[3], r[7], r[8], r[11], r[13], r[10], r[12], r[14], r[15])
	if err != nil {
		panic(err)
	}
}

func formatDate(in string) string {
	months := map[string]string{
		"Jan": "01",
		"Feb": "02",
		"Mar": "03",
		"Apr": "04",
		"May": "05",
		"Jun": "06",
		"Jul": "07",
		"Aug": "08",
		"Sep": "09",
		"Oct": "10",
		"Nov": "11",
		"Dec": "12",
	}

	exp := regexp.MustCompile(`(\w\w\w)\s(\d\d)\s'(\d\d)`)
	components := exp.FindStringSubmatch(in)

	return fmt.Sprintf("20%s-%s-%s", components[3], months[components[1]], components[2])
}

func formatName(in string) string {
	return strings.Replace(in, " ", "_", -1)
}

func expandName(in string) string {
	return nameExpansions[in]
}

func fixOTScore(r []string) {
	if len(r[3]) == 0 {
		return
	}

	if r[2] == "W" {
		home, _ := strconv.Atoi(r[7])
		r[7] = strconv.Itoa(home + 1)
	} else {
		visitor, _ := strconv.Atoi(r[8])
		r[8] = strconv.Itoa(visitor + 1)
	}
}

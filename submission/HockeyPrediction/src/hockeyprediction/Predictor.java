package hockeyprediction;

import java.sql.SQLException;

public interface Predictor {
	public double[] predict(String team1, String team2) throws SQLException;
}

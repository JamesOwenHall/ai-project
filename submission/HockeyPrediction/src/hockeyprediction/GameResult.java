package hockeyprediction;

import java.time.LocalDate;

public class GameResult {
	public LocalDate date;
	public String homeTeam;
	public String visitingTeam;
	public int homeGoals;
	public int visitingGoals;
	
	public String toString() {
		return "{Date: " + date + "; Home Team: " + homeTeam + "; Visiting Team: " + visitingTeam + "; Home Goals: " + homeGoals + "; Visiting Goals: " + visitingGoals + "}";
	}
}

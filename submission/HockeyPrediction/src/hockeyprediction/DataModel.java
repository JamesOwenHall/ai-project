package hockeyprediction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Properties;

public class DataModel {
	private final String DBADDR = "localhost";
	private final String DBPORT = "3306";
	private final String DBNAME = "ainn";
	private final String DBUSER = "root";
	private final String DBPASS = "";
	private Connection connection;
	
	public DataModel() throws SQLException {
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", DBUSER);
		connectionProperties.put("password", DBPASS);
		
		String connectionURL = "jdbc:mysql://" + DBADDR + ":" + DBPORT + "/" + DBNAME; 
		
		connection = DriverManager.getConnection(connectionURL, connectionProperties);
	}
	
	public String[] getTeamNames() throws SQLException {
		String query = 
				"SELECT name " +
				"FROM team " +
				"ORDER BY name";
		
		PreparedStatement stmt = connection.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		
		LinkedList<String> result = new LinkedList<String>();
		
		while (rs.next()) {
			result.add(rs.getString("name"));
		}
		
		stmt.close();
		
		return result.toArray(new String[0]);
	}
	
	public boolean teamExists(String team) throws SQLException {
		String query = 
				"SELECT COUNT(*) " +
				"FROM team " +
				"WHERE name = ?";
		
		PreparedStatement stmt = connection.prepareStatement(query);
		stmt.setString(1, team);
		
		ResultSet rs = stmt.executeQuery();
		
		rs.next();
		int count = rs.getInt(1);
		
		return count > 0;
	}
	
	public GameResult[] getGames(String team, LocalDate date) throws SQLException {
		String dateConstraint = "";
		if (date != null) {
			dateConstraint = "AND gh.date < '" + date.toString() + "' ";
		}
		
		String query = 
				"SELECT gh.date, t1.name AS `home_team`, t2.name AS `visiting_team`, gh.home_goals, gh.visiting_goals " +
				"FROM game_history gh, team t1, team t2 " +
				"WHERE gh.home_team_id = t1.id AND gh.visiting_team_id = t2.id AND (t1.name = ? OR t2.name = ?) " +
				dateConstraint +
				"ORDER BY date";
		
		PreparedStatement stmt = connection.prepareStatement(query);
		stmt.setString(1, team);
		stmt.setString(2, team);
		
		ResultSet rs = stmt.executeQuery();
		GameResult[] results = convertToGameResults(rs);
		
		stmt.close();
		return results;
	}
	
	public GameResult[] getHomeGames(String team, LocalDate date) throws SQLException {
		String dateConstraint = "";
		if (date != null) {
			dateConstraint = "AND gh.date < '" + date.toString() + "' ";
		}
		
		String query = 
				"SELECT gh.date, t1.name AS `home_team`, t2.name AS `visiting_team`, gh.home_goals, gh.visiting_goals " +
				"FROM game_history gh, team t1, team t2 " +
				"WHERE gh.home_team_id = t1.id AND gh.visiting_team_id = t2.id AND t1.name = ? " +
				dateConstraint +
				"ORDER BY date";
		
		PreparedStatement stmt = connection.prepareStatement(query);
		stmt.setString(1, team);
		
		ResultSet rs = stmt.executeQuery();
		GameResult[] results = convertToGameResults(rs);
		
		stmt.close();
		return results;
	}
	
	private GameResult[] convertToGameResults(ResultSet rs) throws SQLException {
		LinkedList<GameResult> result = new LinkedList<GameResult>();
		
		while (rs.next()) {
			GameResult gr = new GameResult();
			gr.date = rs.getDate("date").toLocalDate();
			gr.homeTeam = rs.getString("home_team");
			gr.visitingTeam = rs.getString("visiting_team");
			gr.homeGoals = rs.getInt("home_goals");
			gr.visitingGoals = rs.getInt("visiting_goals");
			
			result.add(gr);
		}
		
		return result.toArray(new GameResult[0]);
	}
	
	public int getTeamID(String team) throws SQLException {
		String query = 
				"SELECT id " +
				"FROM team " +
				"WHERE name = ?";
		
		PreparedStatement stmt = connection.prepareStatement(query);
		stmt.setString(1, team);
		ResultSet rs = stmt.executeQuery();
		
		rs.next();
		int id = rs.getInt("id");
		
		stmt.close();
		return id;
	}
}

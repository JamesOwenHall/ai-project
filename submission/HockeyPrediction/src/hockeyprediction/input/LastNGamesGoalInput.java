package hockeyprediction.input;

import hockeyprediction.DataModel;
import hockeyprediction.GameResult;

import java.sql.SQLException;

public class LastNGamesGoalInput implements PredictionInput {
	public int n = 5;
	DataModel model;
	
	public LastNGamesGoalInput(DataModel model) {
		this.model = model;
	}
	
	@Override
	public double generateValue(String team, GameResult[] results, int index) throws SQLException {
		double result = 0;
		
		for (int i = index - n; i < index; i++) {
			if (i < 0) {
				continue;
			}
			
			if (results[i].homeTeam.equals(team)) {
				result += (double)results[i].homeGoals / ((double)n * 10.0);
			} else {
				result += (double)results[i].visitingGoals / ((double)n * 10.0);
			}
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return "Last N games goals";
	}
}

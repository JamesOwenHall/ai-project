package hockeyprediction;

import hockeyprediction.input.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Hashtable;

import org.neuroph.core.data.DataSet;
import org.neuroph.nnet.MultiLayerPerceptron;

public class MultiPredictor implements Predictor {
	LocalDate simDate;
	DataModel model;
	Hashtable<String, MultiLayerPerceptron> networks;
	PredictionInput[] inputs;
	
	public MultiPredictor(DataModel model, PredictionInput[] inputs, LocalDate simDate) throws SQLException {
		this.model = model;
		this.inputs = inputs;
		this.simDate = simDate;
		this.networks = new Hashtable<String, MultiLayerPerceptron>();
	}
	
	public double[] predict(String team1, String team2) throws SQLException {
		loadNetwork(team1);
		loadNetwork(team2);
		
		MultiLayerPerceptron net1 = networks.get(team1);
		MultiLayerPerceptron net2 = networks.get(team2);
		
		double[] net1Input = new double[inputs.length];
		double[] net2Input = new double[inputs.length];
		
		GameResult[] team1Games = model.getGames(team1, simDate);
		GameResult[] team2Games = model.getGames(team2, simDate);
		
		for (int i = 0; i < inputs.length; i++) {
			net1Input[i] = inputs[i].generateValue(team1, team1Games, team1Games.length - 1);
			net2Input[i] = inputs[i].generateValue(team2, team2Games, team2Games.length - 1);
		}
		
		net1.setInput(net1Input);
		net2.setInput(net2Input);
		
		net1.calculate();
		net2.calculate();

		double out1 = net1.getOutput()[0] * 10;
		double out2 = net2.getOutput()[0] * 10;
		
		return new double[]{out1, out2};
	}
	
	private void loadNetwork(String team) throws SQLException {
		DataSet trainingData = generateDataSet(team);
		networks.put(team, Trainer.getNetwork(trainingData));
	}
	
	private DataSet generateDataSet(String team) throws SQLException {
		GameResult[] games = model.getGames(team, HockeyPrediction.TrainingDate);
		DataSet dataSet = new DataSet(inputs.length, 1);
		
		for (int i = 0; i < games.length; i++) {
			if (!games[i].date.isBefore(simDate)) {
				break;
			}
			
			double[] input = new double[this.inputs.length];
			double[] output = new double[1];
			
			for (int j = 0; j < input.length; j++) {
				input[j] = this.inputs[j].generateValue(team, games, i);
			}
			
			if (games[i].homeTeam.equals(team)) {
				output[0] = games[i].homeGoals / 10f;
			} else {
				output[0] = games[i].visitingGoals / 10f;
			}
			
			dataSet.addRow(input, output);
		}
		
		return dataSet;
	}
}

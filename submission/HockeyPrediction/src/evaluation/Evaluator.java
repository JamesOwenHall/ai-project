package evaluation;

import hockeyprediction.*;
import hockeyprediction.input.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Evaluator {
	DataModel model;
	PredictionInput[] allInputs;
	String[] teams;
	double threshold = 1.0;
	
	public Evaluator() throws SQLException {
		model = new DataModel();
		allInputs = PredictionInput.allInputs(model);
		teams = model.getTeamNames();
	}
	
	public void runEvaluation() throws SQLException {
		int[][] inputCombinations = Utils.powerSet(allInputs.length);
		
		for (int i = 0; i < inputCombinations.length; i++) {
			if (inputCombinations[i].length < 2) {
				continue;
			}
			
			PredictionInput[] inputSubset = getSubset(allInputs, inputCombinations[i]);
			
			ArrayList<ResultError> errors = evaluateInputs(inputSubset);
			System.out.println(Arrays.toString(inputSubset));
			printErrors(errors);
		}
	}
	
	private void printErrors(ArrayList<ResultError> errors) {
		int goalErrors = 0;
		int correctWinLoss = 0;
		int inconclusive = 0;
		
		for (ResultError err : errors) {
			goalErrors += Math.abs(err.actualHomeGoals - err.predictedHomeGoals);
			goalErrors += Math.abs(err.actualVisitingGoals - err.predictedVisitingGoals);
			
			if (Math.abs(err.predictedHomeGoals - err.predictedVisitingGoals) < threshold) {
				inconclusive++;
			} else if (err.actualHomeGoals > err.actualVisitingGoals && err.predictedHomeGoals > err.predictedVisitingGoals) {
				correctWinLoss++;
			} else if (err.actualHomeGoals < err.actualVisitingGoals && err.predictedHomeGoals < err.predictedVisitingGoals) {
				correctWinLoss++;
			}
		}
		
		float averageError = (float)goalErrors / (float)errors.size();
		float percentCorrectWinner = 100.0f * (float)correctWinLoss / (float)(errors.size() - inconclusive);
		float percentInconclusive = 100.0f * (float)inconclusive / (float)errors.size();
		
		System.out.println("Average error: " + averageError + " goals/game");
		System.out.println("Correct Winner: " + percentCorrectWinner + "%");
		System.out.println("Inconclusive: " + percentInconclusive + "%");
		System.out.println();
	}

	private PredictionInput[] getSubset(PredictionInput[] allInputs, int[] indexes) {
		PredictionInput[] result = new PredictionInput[indexes.length];
		
		for (int i = 0; i < indexes.length; i++) {
			result[i] = allInputs[indexes[i]];
		}
		
		return result;
	}
	
	private ArrayList<ResultError> evaluateInputs(PredictionInput[] inputs) throws SQLException {
		ArrayList<ResultError> errors = new ArrayList<>();
		
		String[] teams = model.getTeamNames();
		MultiPredictor predictor = new MultiPredictor(model, inputs, LocalDate.now());
		
		for (int i = 0; i < teams.length; i++) {
			errors.addAll(evaluateTeam(predictor, teams[i]));
		}
		
		return errors;
	}
	
	private ArrayList<ResultError> evaluateTeam(MultiPredictor predictor, String team) throws SQLException {
		ArrayList<ResultError> errors = new ArrayList<>();
		
		GameResult[] homeGames = model.getHomeGames(team, LocalDate.now());
		for (int i = 0; i < homeGames.length; i++) {
			if (homeGames[i].date.isBefore(HockeyPrediction.TrainingDate)) {
				continue;
			}
			
			double[] prediction = predictor.predict(homeGames[i].homeTeam, homeGames[i].visitingTeam);
			errors.add(error(prediction, homeGames[i]));
		}
		
		return errors;
	}
	
	private ResultError error(double[] prediction, GameResult result) {
		ResultError error = new ResultError();
		error.actualHomeGoals = result.homeGoals;
		error.actualVisitingGoals = result.visitingGoals;
		error.predictedHomeGoals = prediction[0];
		error.predictedVisitingGoals = prediction[1];
		
		return error;
	}
	
	public static void main(String[] args) {
		try {
			Evaluator evaluator = new Evaluator();
			evaluator.runEvaluation();
		} catch (SQLException e) {
			System.err.println("Unable to connect to database");
		}
	}
}

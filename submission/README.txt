EDAN50 Intelligent Systems: Project
by James Hall and Hannes Johansson

Included in this archive are the files required to build and run our project.  We've included both the source files and compiled bytecode files in the HockeyPrediction/ directory.  The project includes 2 runnable Java applications.

1. HockeyPrediction: This is the main application used for generating match predictions.  It is command-based.  For a list of available commands, type "help".
2. Evaluator: This application is used to evaluate the effectiveness of the various combinations of input data we're using to make our predictions.  It takes no user input; results are printed to standard output.

** Setup **

The project requires Java 1.8.0 or later.  The public computers in E-huset are not compatible because they have Java 1.6 installed.  The latest version of Java is available at Oracle's website: http://www.oracle.com/technetwork/java/javase/downloads/index.html

The project also requires a MySQL database to be installed.  This is available from several package managers, as well as from MySQL's website: http://dev.mysql.com/downloads/mysql/

Our applications expect the following from the database:
	- username: root
	- password: <none>
	- db name: ainn
	- port: 3306

Alternatively, these parameters can be easily changed in the file HockeyPrediction/src/hockeyprediction/DataModel.java.
The applications also expect the data from the file db_dump.sql to be inserted into the database.

** Building **

Before building, ensure you have completed the setup step.

The archive includes precompiled files that can be run directly.  However, building our applications again is straight-forward, but it requires including 2 external libraries in the classpath.  From the terminal or command prompt, navigate to the root directory of this archive and enter the following commands:

javac -cp HockeyPrediction/src/:lib/neuroph/neuroph-core-2.8.jar:lib/mysql-connector/mysql-connector-java-5.1.30-bin.jar -d HockeyPrediction/bin/ HockeyPrediction/src/hockeyprediction/*.java

javac -cp HockeyPrediction/src/:lib/neuroph/neuroph-core-2.8.jar:lib/mysql-connector/mysql-connector-java-5.1.30-bin.jar -d HockeyPrediction/bin/ HockeyPrediction/src/evaluation/*.java

** Running **

Before running, ensure you have completed the setup step.  You must also start the MySQL database server.

Running our applications is very similar to the building process.  The two following commands should be used from the root directory of this archive:

java -cp HockeyPrediction/bin/:lib/neuroph/neuroph-core-2.8.jar:lib/mysql-connector/mysql-connector-java-5.1.30-bin.jar hockeyprediction/HockeyPrediction

java -cp HockeyPrediction/bin/:lib/neuroph/neuroph-core-2.8.jar:lib/mysql-connector/mysql-connector-java-5.1.30-bin.jar evaluation/Evaluator

** License **

This project is released under the Gnu General Public License v3.0.  See LICENSE.txt for details.
Copyright (C) 2014 James Hall & Hannes Johansson
